# -*- coding:utf-8 -*-
# @Time : 2021/3/6 6:44 下午
# @Author : hyli
# @File : addressListPage.py
# @Software : PyCharm


# 通讯录页面
from stage6.page.addUserPage import AdduserPage
from stage6.page.basePage import BasePage


class AddressListPage(BasePage):
    def goto_addUserPage(self):
        self.parse_action('../yaml/addressList.yaml')
        return AdduserPage(self.driver)

# -*- coding:utf-8 -*-
# @Time : 2021/3/6 6:40 下午
# @Author : hyli
# @File : mainPage.py
# @Software : PyCharm

# 应用首页
from stage6.page.addressListPage import AddressListPage
from stage6.page.basePage import BasePage


class MainPage(BasePage):
    def goto_addressListPgae(self):
        self.parse_action('../yaml/main.yaml')
        return AddressListPage(self.driver)

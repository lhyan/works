# -*- coding:utf-8 -*-
# @Time : 2021/3/6 6:52 下午
# @Author : hyli
# @File : addUserPage.py
# @Software : PyCharm


# 添加成员页面
# 通过读取userInfo.yaml文件，添加用户信息。userInfo.yaml文件中的infoList存储着用户信息
from stage6.page.basePage import BasePage


class AdduserPage(BasePage):
    def addUser(self):
        self.parse_action('../yaml/userInfo.yaml')
        # self.find_click("//*[@resource-id='com.tencent.wework:id/gur']")

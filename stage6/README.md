一、Apppinum__企业微信-添加成员功能

       1. 进入企业微信首页  关联yaml/main.yaml
       2. 点击"通讯录"
       3. 点击"添加成员"，点击"手动输入添加"    关联yaml/addressList.yaml
       4. 写入成员信息，如：姓名，手机号。点击"保存"按钮   关联yaml/userInfo.yaml
       注释：相关页面要操作的元素以及元素事件都写相关联的yaml文件中
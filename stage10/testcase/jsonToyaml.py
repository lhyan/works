# -*- coding:utf-8 -*-
# @Time : 2021/3/28 10:22 上午
# @Author : hyli
# @File : jsonToyaml.py
# @Software : PyCharm
import json

import yaml

if __name__ == '__main__':
    data = {
        "creat_user": [
            {
                'userid': 'lhy_11',
                'name': 'lhy_11',
                'mobile': '+86 13224356278',
                'department': [1],
            },
            {
                'userid': 'lhy_12',
                'name': 'lhy_12',
                'mobile': '+86 13224356212',
                'department': [1],
            },
            {
                'userid': 'lhy_13',
                'name': 'lhy_13',
                'mobile': '+86 13224356213',
                'department': [1],
            }

        ],
        "get_user": [
            {
                'userid': 'lhy_11',
                'name': 'lhy_11',
                'mobile': '+86 13224356278',
                'department': [1],
            },
            {
                'userid': 'lhy_12',
                'name': 'lhy_12',
                'mobile': '+86 13224356212',
                'department': [1],
            }

        ]

    }
    # 将json数据转化成yaml格式：
    # 方式一：
    # 将数据转换成str
    dataStr = json.dumps(data)
    dyaml = yaml.load(dataStr)  # 将字符转仓yaml
    filey = '/Users/hyli/PycharmProjects/works/stage10/yaml/demo.yaml'
    stream = open(filey, 'w')
    yaml.safe_dump(dyaml, stream, default_flow_style=False)  # 输出到文件中

    # 方式二：
    # 将字符转仓yaml
    # print(yaml.dump(data))

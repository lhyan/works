# -*- coding:utf-8 -*-
# @Time : 2021/3/27 5:00 下午
# @Author : hyli
# @File : test_address.py
# @Software : PyCharm
import pytest
import requests
import yaml

from stage10.common.address_api import Address


class TestAddress:
    yamlData = yaml.safe_load(open('../yaml/address.yaml', 'r', encoding="utf-8"))

    def setup_class(self):
        self.address = Address()

    @pytest.mark.parametrize('data', yamlData['creat_user'])
    # 创建成员
    def test_creat_user(self, data):
        # 创建之前先删除
        self.address.del_user(data['userid'])
        # 创建成员
        creat_result = self.address.creat_user(data)
        # 查询成员，是否可以查询到新增成员
        getUser_result = self.address.get_userInfo(data['userid'])
        # 成员查询成功之后删除成员
        del_result = self.address.del_user(data['userid'])
        assert creat_result.get('errmsg') == 'created' and getUser_result['name'] == data['name'] and del_result[
            'errmsg'] == 'deleted'

    # 查询成员
    @pytest.mark.parametrize('data', yamlData['get_user'])
    def test_get_user(self, data):
        # 创建成员
        creat_result = self.address.creat_user(data)
        # 查询成员，是否可以查询到新增成员
        getUser_result = self.address.get_userInfo(data['userid'])
        # 成员查询成功之后删除成员
        del_result = self.address.del_user(data['userid'])
        assert getUser_result.get('name') == data['name'] and del_result['errmsg'] == 'deleted'

    # 修改成员
    @pytest.mark.parametrize('data', yamlData['get_user'])
    def test_edit_user(self, data):
        # 创建成员
        creat_result = self.address.creat_user(data)
        # 修改成员信息
        data['name'] = data['name'] + "_aa"
        update_result = self.address.edit_userInfo(data)
        # 成员查询成功之后删除成员
        del_result = self.address.del_user(data['userid'])
        assert update_result.get('errmsg') == 'updated' and del_result['errmsg'] == 'deleted'

    # 删除成员
    @pytest.mark.parametrize('data', yamlData['del_user'])
    def test_del_user(self, data):
        # 创建成员
        creat_result = self.address.creat_user(data)
        # 删除成员
        del_result = self.address.del_user(data['userid'])
        # 查询成员，是否可以查询到新增成员
        getUser_result = self.address.get_userInfo(data['userid'])
        assert creat_result.get('errmsg') == 'created' and del_result['errmsg'] == 'deleted'
        assert getUser_result.get('errcode') == 60111

 #### 企业微信接口测试-- 通讯录-增、删、改、查接口测试用例编写。
 #### 1.数据清理  2.处理并发（xdist插件）

### 目录：
    1. yaml
        *.yaml
    2. testcase
        result
          tmp.png
          report.log
        test_address.py
    3. common
         address_api.py
         base.py
    
### 用例描述：
       1. 创建成员
       2. 查询成员
       3. 修改成员信息
       4. 删除成员

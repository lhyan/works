# -*- coding:utf-8 -*-
# @Time : 2021/3/27 4:29 下午
# @Author : hyli
# @File : base.py
# @Software : PyCharm
import requests


class Base():
    def __init__(self, corpsecret):
        self.session = requests.session()
        self.token = self.get_token(corpsecret)
        self.session.params = {"access_token": self.token}

    # 获取用的token信息
    def get_token(self, corpsecret):
        # 企业ID
        corpid = 'wwc64554a41fa91c16'
        # 应用的秘钥凭证（通讯录）
        corpsecret = corpsecret
        url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        params = {"corpid": corpid, "corpsecret": corpsecret}
        r = requests.get(url, params=params)
        token = r.json()['access_token']
        return token

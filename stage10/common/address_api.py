# -*- coding:utf-8 -*-
# @Time : 2021/3/27 4:49 下午
# @Author : hyli
# @File : address_api.py
# @Software : PyCharm

# 通讯录-相关方法
from stage10.common.base import Base


class Address(Base):
    def __init__(self):
        self.baseUrl = "https://qyapi.weixin.qq.com/cgi-bin/user"

        # 根据通讯录的秘钥（secret）获取access_token信息且作为session参数传递给后端
        super().__init__('Zm4kzPrQ-3jqnWvhCAL1jLOqHuNNaK-QM3kWIjKXoGw')

    # 创建成员
    def creat_user(self, jsonData):
        url = self.baseUrl + "/create"
        r = self.session.post(url, json=jsonData)
        return r.json()

    # 查询用户信息
    def get_userInfo(self, userid):
        url = self.baseUrl + "/get"
        params = {"userid": userid}
        r = self.session.post(url, params)
        return r.json()

    # 修改用户信息
    def edit_userInfo(self, jsonData):
        url = self.baseUrl + "/update"
        r = self.session.post(url, json=jsonData)
        return r.json()

    # 删除用户
    def del_user(self, userId):
        url = self.baseUrl + "/delete"
        params = {"userId": userId}
        r = self.session.post(url, params)
        return r.json()

# -*- coding:utf-8 -*-
"""
原有存款 1000元， 发工资之后存款变为2000元
定义模块
1、money.py saved_money = 1000
2、定义发工资模块 send_money，用来增加收入计算
3、定义工资查询模块 select_money，用来展示工资数额
4、定义一个start.py ，启动文件展示最终存款金额

"""

import money
from select_money import select_money
from send_money import send_money

# 启动页

if __name__ == '__main__':
    print(f'原有存款：{money.saved_money}')

    # 增加收入
    send_money(1000)
    print(f'现有存款：{money.saved_money}')

    # 展示工资
    select_money()
    print(f'发的工资金额：{select_money()}')

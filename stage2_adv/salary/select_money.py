# -*- coding:utf-8 -*-

# from money import saved_money
# from money import send_salary

import money


# 查询工资  前提条件：必须的先发工资，否则返回值为0 。既没有发工资

def select_money():
    return money.send_salary


if __name__ == '__main__':
    select_money()

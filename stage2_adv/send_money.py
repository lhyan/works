"""
原有存款 1000元， 发工资之后存款变为2000元
定义模块
1、money.py saved_money = 1000
2、定义发工资模块 send_money，用来增加收入计算
3、定义工资查询模块 select_money，用来展示工资数额
4、定义一个start.py ，启动文件展示最终存款金额

"""

# saved_money:默认存款 send_salary:薪资额度
saved_money = 1000
send_salary = 0


# 发工资
def send_money(salary):
    global send_salary
    global saved_money
    send_salary = salary
    saved_money += salary
    return saved_money


# 查询工资  前提条件：必须的先发工资，否则返回值为0 。既没有发工资
def select_money():
    return send_salary


if __name__ == '__main__':
    print(f'原有存款：{saved_money}')

    # 增加收入
    send_money(1000)
    print(f'现有存款：{saved_money}')

    # 展示工资
    select_money()
    print(f'发的工资金额：{select_money()}')

# -*- coding:utf-8 -*-
# @Time : 2021/2/27 10:29 下午
# @Author : hyli
# @File : test_weixin.py
# @Software : PyCharm
import shelve

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class TestWeixin:
    def setup(self):
        # 复用chrome
        # Mac启动命令：
        # / Applications / Google\ Chrome.app / Contents / MacOS / Google\ Chrome - -remote - debugging - port = 9222
        # option = Options()
        # option.debugger_address = "127.0.0.1:9222"
        # self.driver =webdriver.Chrome(options=option)

        # 不复用chrome
        self.driver = webdriver.Chrome()
        # 隐式等待
        self.driver.implicitly_wait(5)

    @pytest.mark.skip
    # 用例一
    def test_cookie(self):
        # get_cookies()：获取当前页面的cookie信息
        # cookies = self.driver.get_cookies()
        # print(cookies)
        # 第一次打开 index页面 这时候需要登录
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

        # 通过self.driver.get_cookies()获取到当前页面所拥有的cookie信息
        cookies = [
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False,
             'value': '1688851339942110'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False,
             'value': '1970324962431377'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False,
             'value': 'OK0AK4i9gBYsO--SQNLC0NmtQ0MoGDaW29FiAZgkf6bEVmBotaUHxR9e2wB1Y_Vc'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False,
             'value': 'a2153190'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False,
             'value': '1MPM7YjlxmwJNU17_4cTQab3yZuaypgh6lfvMruhOI8m1svQv6hGv6W-0rmgqoOBHDNz8C4JBqDPsWZlzF4YVhLO8docGDF-AURw7rWlsGIne-kKk_2DMGE34XMRilSejP4Blq6xrdGsepQsG5zksWFH51Cqy3LQZjP7umdVD-uKcrGboif-XXUBKZGvqopWOaW_6kbIXX5cFXQYkIDq3SPhk3RkdE5_xbcv-MLEiI1c3SVJ43HNRwnlWq8pwaXE_XGMz_S4357R_ZsR7CDxLA'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'Hm_lpvt_9364e629af24cb52acc78b43e8c9f77d',
             'path': '/', 'secure': False, 'value': '1614436997'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': False,
             'value': '39669899372385765'},
            {'domain': '.qq.com', 'expiry': 1614524363, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False,
             'value': 'GA1.2.587029008.1614436997'},
            {'domain': 'work.weixin.qq.com', 'expiry': 1614468320, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/',
             'secure': False, 'value': 'unoloa'},
            {'domain': '.qq.com', 'expiry': 1677509963, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False,
             'value': 'GA1.2.2000011943.1614138404'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1645674392, 'httpOnly': False, 'name': 'wwrtx.c_gdpr',
             'path': '/', 'secure': False, 'value': '0'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1617029966, 'httpOnly': False, 'name': 'wwrtx.i18n_lan',
             'path': '/', 'secure': False, 'value': 'zh'},
            {'domain': '.qq.com', 'expiry': 1929431166, 'httpOnly': False, 'name': 'pac_uid', 'path': '/',
             'secure': False, 'value': '0_da25b9c7597c9'},
            {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'ptcz', 'path': '/', 'secure': False,
             'value': '8e67c28e39a09360336eab8f6511f4370333510a4ced3c14cba809b4bad2c6b0'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False,
             'value': '1688851339942110'},
            {'domain': '.qq.com', 'expiry': 1929431166, 'httpOnly': False, 'name': 'iip', 'path': '/', 'secure': False,
             'value': '0'}, {'domain': '.work.weixin.qq.com', 'expiry': 1645972997, 'httpOnly': False,
                             'name': 'Hm_lvt_9364e629af24cb52acc78b43e8c9f77d', 'path': '/', 'secure': False,
                             'value': '1614138404,1614436997'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False,
             'value': '1'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': False,
             'value': 'direct'},
            {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'RK', 'path': '/', 'secure': False,
             'value': 'GYa58eG70Z'},
            {'domain': '.qq.com', 'expiry': 1616754768, 'httpOnly': False, 'name': 'ptui_loginuin', 'path': '/',
             'secure': False, 'value': '2782082132'}]

        # 将页面所需的cookie信息写入
        for cookie in cookies:
            # 删除 cookies 中的 expiry属性， 删除原因：expiry(过期时间)的值可能是浮点数类型，add_cookie()方法中不支持浮点数
            # if "expiry" in cookie.keys():
            #     cookie.pop("expiry")
            self.driver.add_cookie(cookie)

        # 再次打开index页面 可以正常访问，不需要登录（原因：cookie信息已写入）
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

    """
        测试用例二  --- 导入联系人
        1.在首页点击"导入通讯录"按钮
        2.找到"上传文件"元素
        3.上传文件
        4.断言上传的文件名称与页面中上传的附件名称是否一致
    """

    @pytest.mark.skip
    def test_importContacts(self):
        # 第一次打开 index页面 这时候需要登录
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

        # 通过self.driver.get_cookies()获取到当前页面所拥有的cookie信息
        cookies = [
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False,
             'value': '1688851339942110'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False,
             'value': '1970324962431377'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False,
             'value': 'OK0AK4i9gBYsO--SQNLC0NmtQ0MoGDaW29FiAZgkf6bEVmBotaUHxR9e2wB1Y_Vc'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False,
             'value': 'a2153190'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False,
             'value': '1MPM7YjlxmwJNU17_4cTQab3yZuaypgh6lfvMruhOI8m1svQv6hGv6W-0rmgqoOBHDNz8C4JBqDPsWZlzF4YVhLO8docGDF-AURw7rWlsGIne-kKk_2DMGE34XMRilSejP4Blq6xrdGsepQsG5zksWFH51Cqy3LQZjP7umdVD-uKcrGboif-XXUBKZGvqopWOaW_6kbIXX5cFXQYkIDq3SPhk3RkdE5_xbcv-MLEiI1c3SVJ43HNRwnlWq8pwaXE_XGMz_S4357R_ZsR7CDxLA'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'Hm_lpvt_9364e629af24cb52acc78b43e8c9f77d',
             'path': '/', 'secure': False, 'value': '1614436997'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': False,
             'value': '39669899372385765'},
            {'domain': '.qq.com', 'expiry': 1614524363, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False,
             'value': 'GA1.2.587029008.1614436997'},
            {'domain': 'work.weixin.qq.com', 'expiry': 1614468320, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/',
             'secure': False, 'value': 'unoloa'},
            {'domain': '.qq.com', 'expiry': 1677509963, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False,
             'value': 'GA1.2.2000011943.1614138404'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1645674392, 'httpOnly': False, 'name': 'wwrtx.c_gdpr',
             'path': '/', 'secure': False, 'value': '0'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1617029966, 'httpOnly': False, 'name': 'wwrtx.i18n_lan',
             'path': '/', 'secure': False, 'value': 'zh'},
            {'domain': '.qq.com', 'expiry': 1929431166, 'httpOnly': False, 'name': 'pac_uid', 'path': '/',
             'secure': False, 'value': '0_da25b9c7597c9'},
            {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'ptcz', 'path': '/', 'secure': False,
             'value': '8e67c28e39a09360336eab8f6511f4370333510a4ced3c14cba809b4bad2c6b0'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False,
             'value': '1688851339942110'},
            {'domain': '.qq.com', 'expiry': 1929431166, 'httpOnly': False, 'name': 'iip', 'path': '/', 'secure': False,
             'value': '0'}, {'domain': '.work.weixin.qq.com', 'expiry': 1645972997, 'httpOnly': False,
                             'name': 'Hm_lvt_9364e629af24cb52acc78b43e8c9f77d', 'path': '/', 'secure': False,
                             'value': '1614138404,1614436997'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False,
             'value': '1'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': False,
             'value': 'direct'},
            {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'RK', 'path': '/', 'secure': False,
             'value': 'GYa58eG70Z'},
            {'domain': '.qq.com', 'expiry': 1616754768, 'httpOnly': False, 'name': 'ptui_loginuin', 'path': '/',
             'secure': False, 'value': '2782082132'}]

        # 将页面所需的cookie信息写入
        for cookie in cookies:
            # 删除 cookies 中的 expiry属性， 删除原因：expiry(过期时间)的值可能是浮点数类型，add_cookie()方法中不支持浮点数
            # if "expiry" in cookie.keys():
            #     cookie.pop("expiry")
            self.driver.add_cookie(cookie)

        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

        # 点击"导入通讯录" 按钮
        self.driver.find_element_by_css_selector(".index_service_cnt_itemWrap:nth-child(2)").click()
        # 找到"上传文件"按钮，进行文件上传  send_keys()  方法即可以在输入框中输入信息 也可以进行附件上传
        self.driver.find_element_by_id("js_upload_file_input").send_keys(
            "/Users/hyli/Desktop/系统资料/excel/excel创建表导入模板.xls")
        assert "excel创建表导入模板.xls" == self.driver.find_element_by_id("upload_file_name").text

    # 用例三  用例三是用例二的另一种写法  通过shelve：python内置模块，去创建一个小型的数据库，将页面需要的cookie信息存储到小型数据库中，使用cookie信息的时候直接中小型数据库中读取（db[cookie]）
    # 实现 cookie 数据的持久化存储
    def test_shelve(self):
        db = shelve.open('./mydbs/cookies')
        # 像db文件写入内容
        # db['cookie'] = cookies
        # 关闭数据库
        # db.close()
        # 读取db文件中的cookie对应的属性值
        cookies = db['cookie']

        # 第一次打开 index页面 这时候需要登录
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        for cookie in cookies:
            # 删除 cookies 中的 expiry属性， 删除原因：expiry(过期时间)的值可能是浮点数类型，add_cookie()方法中不支持浮点数
            # if "expiry" in cookie.keys():
            #     cookie.pop("expiry")
            self.driver.add_cookie(cookie)

        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

        # 点击"导入通讯录" 按钮
        self.driver.find_element_by_css_selector(".index_service_cnt_itemWrap:nth-child(2)").click()
        # 找到"上传文件"按钮，进行文件上传  send_keys()  方法即可以在输入框中输入信息 也可以进行附件上传
        self.driver.find_element_by_id("js_upload_file_input").send_keys(
            "/Users/hyli/Desktop/系统资料/excel/excel创建表导入模板.xls")

        assert "excel创建表导入模板.xls" == self.driver.find_element_by_id("upload_file_name").text

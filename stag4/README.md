### 需安装如下插件：
* pytest
* PyYAML
* pytest-ordering
* allure-pytest


### 生成测试用例报告命令：
* 生成allure测试结果：pytest -alluredir=./result filename.py
* 展示报告：allure serve ./result
* 生成最终版本的报告：allure generate ./result
* 清除上一次的记录：allure generate --clean result -o result/html
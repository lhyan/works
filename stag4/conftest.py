# -*- coding:utf-8 -*-
import pytest
import yaml

from stag4.python_code.calc import Calculator


@pytest.fixture(scope='module')
def get_calc():
    # print('开始计算')
    calc = Calculator()
    yield calc
    # print('结束计算')


with open("./datas/calc.yaml") as f:
    # 获取整个yaml文件参数
    datas = yaml.safe_load(f)
    print(datas)
    # 获取加法参数
    add_data = datas['add']['datas']
    add_ids = datas['add']['myid']

    # 获取减法参数
    sub_data = datas['sub']['datas']
    sub_ids = datas['sub']['myid']

    # 获取乘法参数
    mul_data = datas['mul']['datas']
    mul_ids = datas['mul']['myid']

    # 获取除法参数
    div_data = datas['div']['datas']
    div_ids = datas['div']['myid']


@pytest.fixture(params=add_data, ids=add_ids)
def get_add_datas(request):
    print("加法计算开始")
    data = request.param
    yield data
    print("加法计算结束")


@pytest.fixture(params=sub_data, ids=sub_ids)
def get_sub_datas(request):
    print("减法计算开始")
    data = request.param
    yield data
    print("减法计算结束")


@pytest.fixture(params=mul_data, ids=mul_ids)
def get_mul_datas(request):
    print("乘法计算开始")
    data = request.param
    yield data
    print("乘法计算结束")


@pytest.fixture(params=div_data, ids=div_ids)
def get_div_datas(request):
    print("除法计算开始")
    data = request.param
    yield data
    print("除法计算结束")

# -*- coding: utf-8 -*-
# @Author : lihy
# @File    : test_calc.py
import allure
import pytest


# 文件名以test_开头， 类名以Test开头， 方法名以test_开头
# 注意：测试类里一定不要加__init__()方法

# 添加测试类名称
@allure.feature('测试计算器')
class TestCalc:
    """
    优化点：
    1.把setup 与 teardown 换成fixture 方法 get_calc
    2.把 get_calc 方法放到 conftest.py文件中
    3.把参数化（@pytest.mark.parametrize）换成了 fixture 参数化方式
    4.测试用例中的数据需要通过 get_datas 获取
    get_datas 的返回值为列表，如：[0.1,0.2,0.3],分别代表了parametrize中的a,b,expect
    """

    # 加法运算
    # 添加测试类-方法名称
    @allure.story('测试加法')
    @pytest.mark.run(order=1)
    # @pytest.mark.add
    def test_add(self, get_calc, get_add_datas):
        # 调用相加方法
        # 添加测试步骤名称
        with allure.step('计算两个数的相加和'):
            result = get_calc.add(get_add_datas[0], get_add_datas[1])
        if isinstance(result, float):
            result = round(result, 2)
        # 得到相加结果之后写断言
        print(f'写入的数据为：{get_add_datas[2]},运行结果为：{result}')
        assert result == get_add_datas[2]

    # 除法运算
    @allure.story('测试除法')
    @pytest.mark.run(order=4)
    @pytest.mark.div
    def test_div(self, get_calc, get_div_datas):
        with allure.step('计算两个数的除法值'):
            result = get_calc.div(get_div_datas[0], get_div_datas[1])
        if isinstance(result, float):
            result = round(result, 2)

        print(f'写入的数据为：{get_div_datas[2]},运行结果为：{result}')
        assert result == get_div_datas[2]

    #  减法运算
    @allure.story('测试减法')
    @pytest.mark.run(order=2)
    @pytest.mark.sub
    def test_sub(self, get_calc, get_sub_datas):
        with allure.step('计算两个数的减法值'):
            result = get_calc.sub(get_sub_datas[0], get_sub_datas[1])
        if isinstance(result, float):
            result = round(result, 2)
        print(f'写入的数据为：{get_sub_datas[2]},运行结果为：{result}')

    #  乘法运算
    @allure.story('测试乘法')
    @pytest.mark.run(order=3)
    @pytest.mark.mul
    def test_mul(self, get_calc, get_mul_datas):
        with allure.step('计算两个数的乘法和'):
            result = get_calc.mul(get_mul_datas[0], get_mul_datas[1])
        if isinstance(result, float):
            result = round(result, 2)
        print(f'写入的数据为：{get_mul_datas[2]},运行结果为：{result}')

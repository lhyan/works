# -*- coding:utf-8 -*-
# @Time : 2021/3/14 11:26 上午
# @Author : hyli
# @File : app.py
# @Software : PyCharm
from appium import webdriver

from stage6_adv.page.main_page import MainPage


class App:
    def __init__(self):
        self.driver = None
        self.start()

    def start(self):
        _package = "com.tencent.wework"
        _activity = "com.tencent.wework.launch.WwMainActivity"
        if self.driver is None:
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '6.0'
            desired_caps['deviceName'] = 'emulator-5554'
            # 包名
            desired_caps['appPackage'] = _package
            # 测试页面
            desired_caps['appActivity'] = _activity
            desired_caps['autoGrantPermission'] = True
            desired_caps['noReset'] = 'true'

            self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
            self.driver.implicitly_wait(5)
        else:
            # 复用driver
            self.driver.start_activity(_package, _activity)
        return self

    def main(self):
        return MainPage(self.driver)

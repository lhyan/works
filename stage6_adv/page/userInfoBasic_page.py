# -*- coding:utf-8 -*-
# @Time : 2021/3/14 4:53 下午
# @Author : hyli
# @File : userInfoBasic_page.py
# @Software : PyCharm
from stage6_adv.page.base_page import BasePage
from stage6_adv.page.userInfoDetail_page import UserInfoDetailPage


class UserInfoBasicPage(BasePage):
    def goto_userInfoDetail(self):
        self.parse_action("../yaml/userInfoBasic_page.yaml", "edit_user")
        return UserInfoDetailPage(self.driver)

# -*- coding:utf-8 -*-
# @Time : 2021/3/14 4:46 下午
# @Author : hyli
# @File : search_page.py
# @Software : PyCharm
from selenium.webdriver.common.by import By

from stage6_adv.page.base_page import BasePage
from stage6_adv.page.userInfoBasic_page import UserInfoBasicPage


class SearchPage(BasePage):
    # 成员搜索结果
    eleResult = []

    # 搜索成员
    def get_search(self, name):
        # root_log.info(f"输入用户名{name}")
        self._params['name'] = name
        self.parse_action('../yaml/search_page.yaml', 'search_user')
        # 使用这种方式获取不到元素，不解原因
        # 方式一
        # self.eleResult = self.parse_action('../yaml/search_page.yaml', 'get_result')

        # 方式二
        self.eleResult = self.finds('xpath', '//*[@resource-id="com.tencent.wework:id/avi"]')
        # 方式三
        # self.eleResult = self.finds(By.XPATH, '//*[@resource-id="com.tencent.wework:id/avi"]')
        return self.eleResult

    # 点击搜索结果中的第一条数据
    def goto_userInfo(self):
        # root_log.info("点击第一个成员")
        if len(self.eleResult) > 0:
            self.eleResult[0].click()
        return UserInfoBasicPage(self.driver)

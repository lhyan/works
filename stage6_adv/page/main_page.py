# -*- coding:utf-8 -*-
# @Time : 2021/3/14 11:26 上午
# @Author : hyli
# @File : main_page.py
# @Software : PyCharm
from stage6_adv.page.addressList_page import AddressListPage
from stage6_adv.page.base_page import BasePage


class MainPage(BasePage):
    def goto_adressList(self):
        self.parse_action("../yaml/main_page.yaml", "goto_addresslist")
        return AddressListPage(self.driver)

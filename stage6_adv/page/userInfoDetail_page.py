# -*- coding:utf-8 -*-
# @Time : 2021/3/14 5:14 下午
# @Author : hyli
# @File : userInfoDetail_page.py
# @Software : PyCharm
from stage6_adv.page.base_page import BasePage


class UserInfoDetailPage(BasePage):
    # 成员搜索结果
    delEleResult = []

    def del_user(self):
        # 使用滑动的方式（ action: swip_click）程序在执行时会报错。报错信息：元素查找不到。 最后改为使用find_click方式
        self.parse_action("../yaml/userInfoDetail_page.yaml", "del_user")
        return self

    def get_delResult(self):
        self.delEleResult = self.finds('xpath', '//*[@resource-id="com.tencent.wework:id/avi"]')
        return self.delEleResult

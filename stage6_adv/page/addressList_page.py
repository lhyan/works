# -*- coding:utf-8 -*-
# @Time : 2021/3/14 4:42 下午
# @Author : hyli
# @File : addressList_page.py
# @Software : PyCharm
from stage6_adv.page.base_page import BasePage
from stage6_adv.page.search_page import SearchPage


class AddressListPage(BasePage):
    def goto_search(self):
        self.parse_action("../yaml/addressList_page.yaml", "goto_search")
        return SearchPage(self.driver)

 #### 打造自己的测试框架。完成删除联系人框架封装，处理异常情况，添加日志收集

### 目录：
    1. page
        *page.py
    2. yaml
        *.yaml
    3. testcase
        result
          tmp.png
          report.log
        test_address.py
    4. conftest.py
    5. pytest.ini
    
### 用例描述：
       1. 进入首页，点击"通讯录"
       2. 通讯录页面点击"搜索"按钮
       3. 在搜索页面输入成员名称
       4. 点击要删除的成员名称，进入"个人信息"页面
       5. 点击"个人信息"页面的右上角的图标，点击"编辑成员"按钮
       6. 在"编辑成员"页面点击"删除成员"按钮.断言删除前搜索的成员len值与删除后的成员len值比较，不相等即删除成功。
### 备注： 
      日志与截屏文件默认保存的位置与运行的文件在同级目录下，如果想指定具体路径需将
      （1）logging方法（conftest.py）中filename='指定的文件目录/report.log',
      （2）self.driver.get_screenshot_as_file(文件指定的路径地址)
       如：self.driver.get_screenshot_as_file('../testcase/result/tmp.png')
       
      收集测试结果：pytest test_address.py --alluredir ../result
      查看测试报告：allure server ../result
      注释：alluredir生成的报告文件夹result与存放的日志与截图文件夹result并不是同一个文件夹。alluredir生成的报告文件夹result与testcase同级，用于存放测试报告。而存放的日志与截图文件夹result是属于testcase子级。
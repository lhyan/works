# -*- coding:utf-8 -*-
# @Time : 2021/3/14 5:23 下午
# @Author : hyli
# @File : test_address.py
# @Software : PyCharm
from stage6_adv.page.app import App


class TestAddress:
    # 成员搜索结果
    eleResult = []
    # 删除成员之后的搜索结果
    delEleResult = []

    def setup(self):
        self.app = App()

    def test_delUser(self):
        searchObj = self.app.main().goto_adressList().goto_search()
        self.eleResult = searchObj.get_search('222')
        self.delEleResult = searchObj.goto_userInfo().goto_userInfoDetail().del_user().get_delResult()
        assert len(self.eleResult) != len(self.delEleResult)

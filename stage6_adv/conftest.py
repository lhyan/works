# -*- coding:utf-8 -*-
# @Time : 2021/3/15 5:15 下午
# @Author : hyli
# @File : conftest.py
# @Software : PyCharm

import logging

root_log = logging.getLogger(__name__)
for h in root_log.handlers[:]:
    root_log.removeHandler(h)
    h.close()

logging.basicConfig(level=logging.INFO,
                    # 日志格式
                    # 时间、代码所在文件名、代码行号、日志级别名字、日志信息
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    # 打印日志的时间
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    # 日志文件存放的目录（目录必须存在）及日志文件名
                    # filename='report.log',
                    filename='result/report.log',
                    # 打开日志文件的方式
                    filemode='a'
                    )

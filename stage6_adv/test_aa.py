# -*- coding:utf-8 -*-
# @Time : 2021/3/15 4:00 下午
# @Author : hyli
# @File : test_aa.py
# @Software : PyCharm
import logging


def test_log():
    root_log = logging.getLogger(__name__)
    print(root_log.handlers)
    for h in root_log.handlers[:]:
        root_log.removeHandler(h)
        h.close()

    logging.basicConfig(level=logging.INFO)

    logging.info('aaa')

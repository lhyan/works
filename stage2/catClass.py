# -*- coding:utf-8 -*-

from stage2.animalClass import Animal


class Cat(Animal):
    def __init__(self, name, initcry, color='white', age='3', sex='雌'):
        super().__init__(name, initcry, color, age, sex)
        self.hair = 'short haired'
        self.initcry = initcry

    def catchMouse(self):
        print(f'这只{self.age}岁、名为{self.name}的猫可以抓老鼠')

    def cry(self):
        print(f'{self.name}会发出{self.initcry}的叫声')


if __name__ == '__main__':
    cat1 = Cat('咪咪', '喵喵', 'red', '5', '雄')

    # 子类新方法
    cat1.catchMouse()
    # 父类方法
    cat1.run()
    # 改写父类方法
    cat1.cry()

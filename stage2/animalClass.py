# -*- coding:utf-8 -*-

class Animal:
    def __init__(self, name, initcry, color='white', age='3', sex='雌', ):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
        self.__initCry = initcry

    def cry(self):
        print(f'这只名为{self.name}的动物会发出{self.__initCry}的叫声')

    def run(self):
        print(f'一只名为"{self.name}"的{self.age}岁{self.sex}性动物跑的速度很快')


if __name__ == '__main__':
    dog = Animal('小花', '旺旺', '', '5', '雄')
    dog.run()

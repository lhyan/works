# -*- coding:utf-8 -*-
# @Time : 2021/2/28 6:02 下午
# @Author : hyli
# @File : test_addMember.py
# @Software : PyCharm
import pytest

from stage5.page.index_page import IndexPage


class TestMember:
    def setup(self):
        self.indexPage = IndexPage()

    def teardown(self):
        self.indexPage.goto_index()

    # 一次添加一个联系人
    @pytest.mark.skip
    def test_addMember(self):
        name = "name3"
        acctid = "name3"
        phoneNum = "15323456781"
        names = self.indexPage.goto_add_member().add_member(name, acctid, phoneNum).get_member()
        print(names)

    # 一次添加多个联系人
    @pytest.mark.parametrize('name, acctid, phoneNum', [
        ('name_1', 'name_1', '15312345678'),
        ('name_2', 'name_2', '15312345670'),
        ('name_3', 'name_3', '15312345679'),
    ], ids=['name_1', 'name_2', 'name_3'])
    def test_addMembers(self, name, acctid, phoneNum):
        names = self.indexPage.goto_add_member().add_member(name, acctid, phoneNum).get_member()
        print(names)

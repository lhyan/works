# -*- coding:utf-8 -*-
# @Time : 2021/2/28 5:17 下午
# @Author : hyli
# @File : index_page.py
# @Software : PyCharm
from time import sleep

from selenium.webdriver.common.by import By
from stage5.page.add_member_page import AddMemberPage
from stage5.page.base_page import BasePage


class IndexPage(BasePage):
    base_url = "https://work.weixin.qq.com/wework_admin/frame#index"

    # 添加成员
    def goto_add_member(self):
        self.find(By.CSS_SELECTOR, ".index_service_cnt_itemWrap:nth-child(1)").click()
        return AddMemberPage(self.driver)

    # 返回首页
    def goto_index(self):
        self.find(By.ID, "menu_index").click()
        return True

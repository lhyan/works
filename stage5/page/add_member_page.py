# -*- coding:utf-8 -*-
# @Time : 2021/2/28 5:19 下午
# @Author : hyli
# @File : add_member_page.py
# @Software : PyCharm
from selenium.webdriver.common.by import By
from stage5.page.get_member_page import GetMemberPage
from stage5.page.base_page import BasePage


class AddMemberPage(BasePage):
    def add_member(self, username, acctid, phoneNum):
        # 姓名
        self.find(By.ID, "username").send_keys(username)
        # 账号
        self.find(By.ID, "memberAdd_acctid").send_keys(acctid)
        # 手机号
        self.find(By.ID, "memberAdd_phone").send_keys(phoneNum)

        # 点击"保存"操作  注释：页面"js_btn_save"元素个数为2，程序在执行时默认会找到第一个元素然后添加事件
        self.find(By.CSS_SELECTOR, ".js_btn_save").click()

        return GetMemberPage(self.driver)

# -*- coding:utf-8 -*-
# @Time : 2021/2/28 5:16 下午
# @Author : hyli
# @File : base_page.py
# @Software : PyCharm

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    base_url = ""

    def __init__(self, driver: WebDriver = None):
        # 如果浏览器为None,初始化浏览器（driver）
        if driver == None:
            # 复用浏览器
            option = Options()
            option.debugger_address = "127.0.0.1:9222"
            self.driver = webdriver.Chrome(options=option)
            # 创建完driver ， 立刻设置隐式等待
            self.driver.implicitly_wait(5)
        else:
            self.driver = driver

        if self.base_url != "":
            self.driver.get(self.base_url)

    # 定义find方法
    def find(self, locator, value):
        return self.driver.find_element(locator, value)

    # 定义finds方法
    def finds(self, locator, value):
        return self.driver.find_elements(locator, value)

    # 定义显示等待,等待元素可点击
    def wait_for_click(self, timeout, locator):
        WebDriverWait(self.driver, timeout).until(expected_conditions.element_to_be_clickable(locator))

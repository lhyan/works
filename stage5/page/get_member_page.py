# -*- coding:utf-8 -*-
# @Time : 2021/2/28 5:21 下午
# @Author : hyli
# @File : get_member_page.py
# @Software : PyCharm
from selenium.webdriver.common.by import By

from stage5.page.base_page import BasePage


class GetMemberPage(BasePage):
    # 获取"通讯录"页面列表"姓名"数据集合
    def get_member(self):
        # 显示等待，等待页面复选框可点击
        locator = (By.CSS_SELECTOR, ".member_colRight_memberTable_th_Checkbox")
        self.wait_for_click(10, locator)
        # 定义成员姓名集合
        name_list = []
        member_name_el = self.finds(By.CSS_SELECTOR, '.member_colRight_memberTable_td:nth-child(2)')
        # 获取成员名称，将其放入name_list
        for name in member_name_el:
            name_list.append(name.get_attribute('title'))
        return name_list
